DOCUTIL_FLAGS=-s --source-url 'https://gitea.com/chingnux/homepage' 

all: index.htm

%.htm: %.rst
	rst2html5 $(DOCUTIL_FLAGS) $< $@
