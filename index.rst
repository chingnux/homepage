欢迎使用 ChinGNUx
========================

ChinGNUx 是一个面向中文用户定制的操作系统，目标是提供一个简单、高效、自由的 GNU/Linux 使用体验。

当前 ChinGNUx 支持 x86_64 体系结构的计算机。

目前 ChinGNUx 仍在测试阶段，并且使用教程尚未完成，建议用户先熟悉 GNU/Linux 的基本操作。

获取 ChinGNUx
-------------------

可以通过以下方式获取 ChinGNUx 的启动和安装镜像。用此启动镜像启动系统后，可以执行 ``startx`` 进入有中文支持的桌面环境。

- 下载镜像 chingnux-2022.04.01-x86_64.iso

  - `下载链接 <https://file.wehack.space/chingnux/iso/chingnux-2022.04.01-x86_64.iso>`__
  - `OpenPGP签名 <https://file.wehack.space/chingnux/iso/chingnux-2022.04.01-x86_64.iso.sig>`__ （公钥为 vimacs 的个人密钥 https://keys.openpgp.org/search?q=0xEA2DB82FE04A9403）
  - SHA256: 654b5bc0d2b1654a3b90ac154771ba4f0d1870a30a0a41d01403d638cf5e5bf4
  - BLAKE2b: 5bb88b4b06749f73e1542361b5f344d4089a943855ce52a0efe4c021b25dd5f5fc33fb5cdec6e28a490439a8e34ad6d216b032a8e7595cdb3af4ea52dd583c82

- 购买 ChinGNUx USB 启动盘

  - `点此处在淘宝购买 <https://item.taobao.com/item.htm?id=638898401495>`__
  - 如果你不用淘宝，可发送邮件至 chingnux-support@wehack.space 联系 ChinGNUx 作者

- 购买预装 ChinGNUx 的计算机： 请发送邮件至 chingnux-support@wehack.space 咨询

下载镜像文件后，下载 OpenPGP 签名文件后使用 ``gpg`` 验证数字签名，以验证镜像文件的完整性。此外可以用 ``sha256sum`` 验证 SHA256 校验值，用 ``b2sum`` 验证 BLAKE2b 校验值。

安装 ChinGNUx
-------------------

用启动镜像启动计算机后，可以通过以下方式安装 ChinGNUx:

- 方式一：执行 ``setup-chingnux`` 并按照其中的指引安装
- 方式二：执行 ``chingnux-install`` 使用基于 archinstall 的安装向导安装

安装过程需要从互联网下载大量数据，请确保安装时有足够快速的网络。

此外，也可以用 Arch Linux 的启动镜像启动后，使用以下 ChinGNUx 的安装脚本安装 ChinGNUx.

- `setup-chingnux <https://gitea.com/chingnux/chingnux-live/raw/branch/chingnux/setup-chingnux>`__

ChinGNUx 的含义
-------------------

ChinGNUx 是 *Chinese* 和 *LiGNUx* 的合成词，构词方式类似于 `Chinglish <https://zh.wikipedia.org/wiki/%E4%B8%AD%E5%BC%8F%E8%8B%B1%E8%AF%AD>`__. 其中 LiGNUx 是 RMS 曾经建议的对 GNU/Linux 操作系统的称呼。[#LiGNUx]_

对 ChinGNUx 一词的建议的拼写方法是将首字母 C 和 GNU 大写，其他字母小写，或为了方便，将所有字母小写拼写为 *chingnux*.

ChinGNUx 是国产操作系统吗？
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

不是。ChinGNUx 是一个 GNU/Linux 发行版，由大量独立的软件包组成，每个软件包由来自世界各地的开发者开发，所以它不是某个国家生产的操作系统，而是全世界自由软件贡献者合作的产品。

ChinGNUx 自主可控吗？
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

对用户来说，ChinGNUx 基本上自主可控，因为这个操作系统中的绝大多数软件都是自由软件。

ChinGNUx 安全吗？
~~~~~~~~~~~~~~~~~~~~~~~~

大多数软件都存在漏洞，并且这些漏洞可能在公开之前被利用。和其他操作系统一样，ChinGNUx 由很多软件组成，也有可能存在安全漏洞。

ChinGNUx 和 Arch 共用软件仓库，可以及时获得 Arch 的软件更新，从而减少安全漏洞被利用的机会。

什么是自由软件？
~~~~~~~~~~~~~~~~~~~

“自由软件”（Free Software）表示的是那些尊重用户和社区自由的软件。 [#FS]_ 这里的自由指以下四种基本自由：

- 基于任何目的，按你的意愿运行软件的自由（自由之零）。
- 学习软件如何工作的自由，按你的意愿修改软件以符合你的计算的自由（自由之一）。可访问源代码是此项自由的先决条件。
- 分发软件副本的自由，因此你可以帮助他人（自由之二）。
- 将你修改过的软件版本再分发给其他人的自由（自由之三）。这样可以让整个社区有机会共享你对软件的改动。可访问源代码是此项自由的先决条件。

什么是 GNU/Linux，它和 Linux 有什么区别？
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

GNU/Linux 是由 GNU 的软件和 Linux 内核组成的操作系统。GNU 是一个操作系统，它包含由 GNU 工程 [#GNU]_ 发布的软件和其他自由软件组成。Linux [#Linux]_ 是由 Linus Torvalds 创作的一个操作系统内核，操作系统的内核的作用包括支持计算机硬件、管理硬件资源、向用户程序提供服务。

因此，Linux 只是操作系统的一部分，完整的操作系统应当称为 GNU/Linux. 为了表达对 GNU 的尊重，请不要实用主义地称整个系统为 "Linux".

ChinGNUx 是一个怎样的发行版？
---------------------------------

ChinGNUx 基于 `Arch Linux <https://www.archlinux.org/>`__ ，一个 `滚动 GNU/Linux 发行版 <https://zh.wikipedia.org/wiki/%E6%BB%BE%E5%8B%95%E7%99%BC%E8%A1%8C>`__ 。ChinGNUx 在 Arch 的基础上加上自己的软件仓库，同时预装中文用户常用的软件，提供适合中文用户的 GNU/Linux 体验。

ChinGNUx 会不会成为一个独立的发行版？
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

当前 ChinGNUx 是一个定制化的 Arch Linux 发行版，类似 `EndeavourOS <https://endeavouros.com/>`__ 等发行版，这样可以及时得到 Arch 的软件更新。ChinGNUx 暂时不考虑做一个独立的发行版。

ChinGNUx 面向哪些用户群体？
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

ChinGNUx 面向所有中文用户。由于大部分用户对 Arch 的操作不熟悉，因此希望用户热爱学习，懂得在遇到问题时查阅文档和 ArchWiki 等资料。

为什么 ChinGNUx 基于 Arch?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

选择 Arch 作为 ChinGNUx 的基础发行版有以下原因：

- 对于桌面用户

  - Arch 有最新的软件
  - Arch 有大量受社区用户欢迎的软件
  - Arch 有高质量的用户社区
  - Arch 有优秀的文档资源 `ArchWiki <https://wiki.archlinux.org>`__

- 对于开发者

  - 由于 Arch 有最新的软件，因此也有最新的开发工具
  - Arch 有一套简单的打包系统，打包脚本基于 bash 脚本，容易编写和维护。
  - Arch 不会为头文件单独拆出一个 -dev 包，简化安装开发者所需软件库的过程

`Arch is the best! <https://wiki.archlinux.org/index.php/Arch_is_the_best>`__

ChinGNUx 和 archlinuxcn 有什么异同？
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

archlinuxcn [#archlinuxcn]_ 是知名的 Arch Linux 中文社区。和 archlinuxcn 相同的是，ChinGNUx 同样提供一个软件仓库。不同的是，ChinGNUx 要定制一套易用的默认桌面，更简单的安装程序。此外，ChinGNUx 的软件仓库还会维护和修正官方仓库已有，但是过期或不可用，并且长期无人维护的软件包。

谁在开发 ChinGNUx?
~~~~~~~~~~~~~~~~~~~~~~~~~~~

当前只有 `vimacs <https://vimacs.wehack.space>`__ 一人。

为什么你不申请 Arch 的 TU?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

如果有 TU 推荐，我会考虑申请。

ChinGNUx 支持哪些 init 和服务管理系统？
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

当前只支持 Arch 官方支持的 systemd. 以后会考虑借鉴 Parabola，支持 OpenRC 等。

ChinGNUx 是否支持可重现构建？
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

可重现构建（reproducible build）用于在不同的机器上构建所有比特完全相同的二进制包，是一个重要的安全特性。

ChinGNUx 也暂无此计划，以后会考虑使用 archlinux-repro 支持可重现构建。

如何使用 ChinGNUx?
------------------------

和其他的 GNU/Linux 发行版一样，很多新用户在使用时，会遇到很多问题。

怎样找到文档？
~~~~~~~~~~~~~~~~~

ChinGNUx 的作者正在写一个系列教程 `《学会使用计算机》 <https://gitea.com/chingnux/learn-to-use-your-computer>`__，但进度缓慢，欢迎大家参与编写。

因为 ChinGNUx 就是 Arch，所以建议大家到 `ArchWiki <https://wiki.archlinux.org>`__ 搜索相关文档。

ChinGNUx 是否提供软件 Y?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

ChinGNUx 除了提供 Arch 提供的软件外，只提供自由软件。对于私有软件，建议用户寻找自由软件的替代品，以下是 GNU/Linux 中的常见软件：

- 办公：LibreOffice 办公生产力套件
- 媒体播放器：mpv
- 文本编辑器：Vim, Emacs, nano, Leafpad
- 浏览器：Firefox
- 邮件客户端：Thunderbird
- 互联网通讯：Pidgin, Gajim, Riot.im
- 图表制作：Dia, draw.io
- 专业画图：Krita
- 图像编辑：GIMP
- 矢量图编辑：Inkscape

在 ChinGNUx 中可以玩游戏吗？
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

GNU/Linux 下有大量自由的游戏。

ChinGNUx 预装的 `Simon Tatham 解谜游戏集 <https://www.chiark.greenend.org.uk/~sgtatham/puzzles/>`__ 包含扫雷、数独等经典解谜游戏。

OpenRA 是经典 RTS 游戏红色警戒的复刻版，它重写了一套自由的游戏引擎，使用原版红警1的资源文件，改善了游戏的平衡性。

0 A.D. 是经典 RTS 游戏帝国时代的复刻版，它的游戏引擎和游戏资源文件全是自由的。

SuperTux 和 SuperTuxKart 是类似于超级马里奥和马里奥卡丁车的游戏，主角是 Linux 的吉祥物 Tux.

我还是想用某 Windows 的软件 Y，怎么办？
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

由于私有软件常常有一些用户难以察觉的恶意行为，因此建议使用虚拟机运行这些软件，保证一定的隔离性。

VirtualBox 是一个易用的虚拟机软件。对于有经验的用户，可以使用 virt-manager.

如果你没钱买 Windows，你可以在虚拟机里装 `Linux Deepin <https://www.deepin.org/>`__ ，它可以运行不少 Windows 下的私有软件。

那么，为什么不用 Deepin?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

因为我们不鼓励用户使用私有软件，而 Deepin 将能运行大量 Windows 的私有软件的功能作为特性来吸引用户，事实上这不是一个好的特性。为了自己的自由，我们应该拒绝使用这些预装大量私有软件的发行版。

如果某私有软件 Y 有 GNU/Linux 版本，我还应该用虚拟机吗？
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

有些私有软件，如 Matlab，还有 Vivado 等 EDA 软件，它们提供 GNU/Linux 版本，通常可以直接运行。但是为了用户的安全和隐私，还是建议隔离使用，使用虚拟机是一个方法，也可以使用其他的隔离方式，如使用容器等轻量的虚拟化方式（如 firejail），使用另一个用户运行，在另一台计算机上运行。

如果不用 QQ、微信、Skype、WhatsApp、...，怎么和别人交流？
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

XMPP 和 Matrix 都是自由的、去中心化的用于即时通讯的协议，你可以在 [#XMPP-List]_ 和 [#Matrix-List]_ 找到公开的 XMPP 和 Matrix 服务器，并用 Gajim 和 Riot.im 进行通讯。同时，请让身边的人也使用它们。

如果他们不会用，你还可以使用电子邮件。


如何为 ChinGNUx 做贡献？
------------------------------

ChinGNUx 正在将源码迁移至 NotABug.org 上。当前部分工程的源码在 gitea.com, gitlab.com 和 GitHub 上，大家可以提交工单和合并请求来贡献此项目。

- NotABug.org: https://notabug.org/chingnux
- Gitea: https://gitea.com/chingnux
- GitLab: https://gitlab.com/chingnux
- GitHub: https://github.com/chingnux

- 软件仓库: https://notabug.org/chingnux/repo

如果我不用这些git服务怎么办？
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

可以通过电子邮件联系我。此外可以在这些地方讨论 ChinGNUx 相关话题：

- `WeHack BBS <https://bbs.wehack.space/>`__ 的 GNU/Linux 讨论区
- Matrix 聊天室： `#chingnux:matrixim.cc <https://matrix.to/#/#chingnux:matrixim.cc>`__


.. [#LiGNUx] 参见 Wikipedia 页面 `GNU/Linux naming controversy <https://en.wikipedia.org/wiki/GNU/Linux_naming_controversy>`__
.. [#GNU] https://fsfs-zh.readthedocs.io/thegnuproject/
.. [#Linux] https://kernel.org/
.. [#FS] https://fsfs-zh.readthedocs.io/free-sw/
.. [#XMPP-List] https://list.jabber.at/
.. [#Matrix-List] https://www.hello-matrix.net/public_servers.php
.. [#archlinuxcn] https://www.archlinuxcn.org/
